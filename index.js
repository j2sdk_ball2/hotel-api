require('dotenv').config({
    path: 'env/.local',
    silent: true
})
const server = require('./src/server')
server.start()