require('dotenv').config({
    path: '.localenv',
    silent: true
})

const server = require('../../src/server')

async function start() {
    if (!start.instance) {
        console.log('starting server ...')

        try {
            const instance = await server.start()
            console.log('starting server success')
            start.instance = instance.listener
            return start
        } catch (error) {
            console.log('starting server failed')
            console.log(`error: ${JSON.stringify(error)}`)
            return null
        }
    }
    return start
}

module.exports = {
    start
}