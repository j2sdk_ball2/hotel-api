const request = require('supertest')
const { expect } = require('chai')
const { routes } = require('../../../config/api')
const path = routes.index
let server

describe('test/integration', () => {
    describe(`#${path}`, () => {

        before(async () => {
            server = await require('../index').start()
        })

        it('Should return 200 (OK) with path /', async () => {

            await request(server.instance)
                .get(path)
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .expect(200)
                .expect(response => {
                    const body = JSON.parse(response.text)
                    expect(body).eql({
                        "name": "backend-developer-assignment-v2",
                        "version": "1.0.0",
                        "description": "The system you are about to develop is intended to handle bids on hotel auction.",
                        "main": "index.js",
                        "repository": {
                            "type": "git",
                            "url": "git+https://github.com/HQInterview/backend-developer-assignment-v2.git"
                        },
                        "keywords": [],
                        "author": "",
                        "license": "ISC",
                        "homepage": "https://github.com/HQInterview/backend-developer-assignment-v2#readme"
                    })
                })
        })

        it('Should return 200 (OK) with path /api/v1/', async () => {

            await request(server.instance)
                .get('/api/v1/')
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .expect(200)
                .expect(response => {
                    const body = JSON.parse(response.text)
                    expect(body).eql({
                        "name": "backend-developer-assignment-v2",
                        "version": "1.0.0",
                        "description": "The system you are about to develop is intended to handle bids on hotel auction.",
                        "main": "index.js",
                        "repository": {
                            "type": "git",
                            "url": "git+https://github.com/HQInterview/backend-developer-assignment-v2.git"
                        },
                        "keywords": [],
                        "author": "",
                        "license": "ISC",
                        "homepage": "https://github.com/HQInterview/backend-developer-assignment-v2#readme"
                    })
                })
        })
    });
})