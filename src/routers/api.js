const _package = require('../../package')
const { routes } = require('../../config/api')
const controller = require('../controllers/index')
const auctionController = require('../controllers/auction')
const roomController = require('../controllers/room')

module.exports = (koaRouter) => {
    koaRouter.get('/', controller.get)
    koaRouter.get(routes.index, controller.get)
    koaRouter.get(routes.auctions, auctionController.get)
    koaRouter.get(routes.rooms, roomController.get)
    return koaRouter
}