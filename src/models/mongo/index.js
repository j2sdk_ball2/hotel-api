const mongoose = require('mongoose')
const Log = require('../../services/log')
const log = new Log(process.cwd(), __filename)
const logInfo = log.info
const logError = log.error
const { mongoUrl } = require('../../../config/db')

mongoose.Promise = require('bluebird')

module.exports = Mongo

// readyState:
// 0 = disconnected
// 1 = connected
// 2 = connecting
// 3 = disconnecting
function Mongo() {
    if (mongoose.connection.readyState === 0) {
        connect()
    }
    return mongoose
}

function connect() {
    try {
        logInfo(Mongo.name, 'connecting to mongodb ...')
        const connection = mongoose.connect(mongoUrl)
        logInfo(Mongo.name, 'mongodb connected')
        return connection
    } catch (error) {
        logError(Mongo.name, error)
        throw error
    }
}