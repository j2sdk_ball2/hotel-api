const Log = require('./services/log')
const log = new Log(process.cwd(), __filename)
const logInfo = log.info
const logError = log.error

const Koa = require('koa')
const koaRouter = require('koa-router')()
const bodyParser = require('koa-bodyparser')
const validator = require('koa-validate')

const apiRouter = require('./routers/api')
const { port } = require('../config/env')
const app = new Koa()

module.exports = {
    start
}

function start() {
    logInfo(start.name)

    validator(app)
    app.use(bodyParser())
    apiRouter(koaRouter)
    app.use(koaRouter.routes())
    app.use(koaRouter.allowedMethods())
    app.on('error', async (error) => {
        logError(start.name, error)
    })
    
    const listener = app.listen(port)
    console.log(`*** server is running on port ${port} ***`)

    return {
        app,
        listener
    }
}