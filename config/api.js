const prefix = '/api/v1'

module.exports = {
    prefix,
    routes: {
        index: `${prefix}/`,
        auctions: `${prefix}/auctions`,
        rooms: `${prefix}/rooms`,
    }
}